
# Movies Similarity Matrix Exploration

Here we will find out how to cretate similarity matrix for movie data.  
The main goal is to find top K similar movies using the similarity matrix we previously create.  
Top K similar movies list should differ depending on input movie.


```python
# load dependencies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import requests
from imdbpie import Imdb
from sklearn.metrics import pairwise_distances,mean_squared_error,mean_absolute_error
```

### Load data

Let's first read data from files.


```python
data_dir = 'data/'
```


```python
# users file
user_columns = ['user_id', 'age', 'sex', 'zip_code']
users = pd.read_csv(data_dir + 'Person.txt', sep='\t', names=user_columns, encoding='latin-1')
```


```python
# ratings file
vote_columns = ['user_id', 'movie_id', 'rating1', 'rating2', 'unix_timestamp']
votes = pd.read_csv(data_dir + 'Vote.txt', sep='\t', names=vote_columns, encoding='latin-1')
```


```python
# movies file
movie_columns = ['movie_id', 'movie_title', 'site_link', 'IMDb_URL', 'release_date', 'release_date_relative', 
                 'video_release_date', 'video_release_date_relative', 'unknown', 'Action', 'Adventure',
                 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy']
movies = pd.read_csv(data_dir + 'Movie.txt', sep='\t', names=movie_columns, encoding='latin-1')
```


```python
# merge users, movies, and votes data
movie_ratings = pd.merge(movies, votes)
merged_table = pd.merge(movie_ratings, users)
```

Since our user's and movie's IDs are not continual, we will need to make dictionaries that will make a relation between user ID and user Index, and movie ID and movie Index in users and movies dictionaries, respectively.  
Then we will be able to easily map *user/movie ID's* to *user/item indices* by using these dictionaries.


```python
user_indices = {user.user_id: user.Index for user in users.itertuples()}
user_by_index = dict((v,k) for k,v in user_indices.iteritems())

movie_indices = {movie.movie_id: movie.Index for movie in movies.itertuples()}
movie_by_index = dict((v,k) for k,v in movie_indices.iteritems())
```

### Explore similarity matrix

Prepare data for making matrix that consists of a user ratings of movies - **user-movie matrix**.  
The position of the *rating* is determined by *user Id* (as 0th axis) and *movie Id* (as 1st axis).


```python
total_users = users.shape[0]
total_movies = movies.shape[0]
total_votes = votes.shape[0]
```


```python
ratings_matrix = np.zeros((total_users, total_movies))
for row in votes.itertuples():
    ratings_matrix[user_indices[row.user_id], movie_indices[row.movie_id]] = row.rating1
    #ratings_matrix[[user.Index for user in users.itertuples() if user.user_id == row.user_id][0], 
                   #[movie.Index for movie in movies.itertuples() if movie.movie_id == row.movie_id][0]] = row.rating1
print(ratings_matrix)
```

    [[ 0.6  0.6  0.  ...,  0.   1.   0. ]
     [ 0.8  0.   0.  ...,  0.   0.   0. ]
     [ 0.   0.   0.  ...,  0.   0.   0. ]
     ..., 
     [ 0.8  0.   0.4 ...,  0.   0.   0. ]
     [ 0.   0.   0.  ...,  0.   0.   0. ]
     [ 0.8  0.   0.  ...,  0.   0.   0. ]]
    

Now, we will split our data into training and test sets by removing 10 random ratings per user (`size=10`) from the training set and place them in the test set. This provides us with a possibility to check whether our recommendation system works appropriately.


```python
def train_test_split(ratings):
    test = np.zeros(ratings.shape)
    train = ratings.copy()
    for user in range(ratings.shape[0]):
        if ratings[user, :].nonzero()[0].size:
            test_ratings = np.random.choice(ratings[user, :].nonzero()[0], size=10, replace=True)
            train[user, test_ratings] = 0.
            test[user, test_ratings] = ratings[user, test_ratings]

    # Test and training are truly disjoint
    assert (np.all((train * test) == 0))
    return train, test
```


```python
train, test = train_test_split(ratings_matrix)
```

Fuction that will return the list of movies that are most similar to the input movie.
Ideally, if random user has rated for all of movies outputed from this fucntion, the error of rating prediction he would give to the input movie should be minimal.


```python
def top_k_movies(similarity, mapper, movie_idx, k=6):
    return [mapper[x] for x in np.argsort(similarity[movie_idx, :])[:-k-1:-1]]
```

Let's calculate similarity matrix using the built-in scikit-learn function. By choosing `metrc='correlation'`, we will be using the *Pearson correlation* to determine item similarity matrix.  


```python
item_correlation = 1 - pairwise_distances(train.T, metric='correlation')
item_correlation[np.isnan(item_correlation)] = 0
```

In order to easily see whether our calculated top K movies seem similar to input movie, we will collect posters for found movies.  
This can help us se if the similarity we use is a good metric to use.  
This is possible to do on this kind of data because we are dealing with a domain where many of us have intuition for what seems right and what seems wrong.  
We can take a look on our movie similarity matrix and see if similar movies make sense.
  
Some of the movies won't have a poster, their movie title will be printed out instead.


```python
def get_poster(id, movie):
    if id == "www.imdb.com":
        return None
    try:
        imdb = Imdb()
        imdb = Imdb(anonymize=True) # to proxy requests
        title = imdb.get_title_by_id(id)
        poster = ''
        for capt in imdb.get_title_images(id):
            if capt.caption == movie.movie_title:
                poster = capt.url

        if poster == '':
            poster = imdb.get_title_images(id)[1].url

    except IndexError:
        return None
    return poster
```

Now, let's see how it works...


```python
from IPython.display import Image
from IPython.display import display

idx = 216
list_top_k_movies = top_k_movies(item_correlation, movie_by_index, idx)

for m in list_top_k_movies:
    print('*'*25)
    print(m.movie_title)
    
    response = requests.get(m.IMDb_URL)
    id_poster = response.url.split('/')[-2]
    poster_URL = get_poster(id_poster, m)
    if poster_URL is not None:
        img = Image(url=poster_URL, width=200, height=200)
        display(img)
    else:
        print('[No poster]')
        
```

    *************************
    Boys on the Side
    


<img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTY3MjM3Nzc1NF5BMl5BanBnXkFtZTcwOTc3NDkyMQ@@._V1_.jpg" width="200" height="200"/>


    *************************
    Circle of Friends
    


<img src="https://images-na.ssl-images-amazon.com/images/M/MV5BNTAwOTIwMjgxMV5BMl5BanBnXkFtZTcwNjQ0MDYyMQ@@._V1_.jpg" width="200" height="200"/>


    *************************
    When a Man Loves a Woman
    


<img src="https://images-na.ssl-images-amazon.com/images/M/MV5BODBhYzRiN2ItYjgxMS00MGUwLTg2ODEtY2UzYWM1NzdlYjQ5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg" width="200" height="200"/>


    *************************
    To Wong Foo, Thanks for Everything! Julie Newmar
    [No poster]
    *************************
    Dolores Claiborne
    [No poster]
    *************************
    Something to Talk About
    


<img src="https://images-na.ssl-images-amazon.com/images/M/MV5BMTk5MzA2NjI0Nl5BMl5BanBnXkFtZTYwNzczMTU5._V1_.jpg" width="200" height="200"/>


### Explore prediction

The following code will be optimized and generalized in the next script.  
However, here we will use it just to see how some basic things work.

Make a list of top K movie indices. Top K movies are calculated previously, and placed inside `list_top_k_movies`.


```python
list_top_k_movies_indexes = []
for m in list_top_k_movies:
    list_top_k_movies_indexes.append(movie_indices[m.movie_id])
```

Since matrix that consists ratings is very sparse, we will find users that rated every movie from top K movies list.  
Of course, the ideal situation will be that all users rated almost every movie from dataset, so we could predict their ratings more accuratelly. We will simulate this situation in order to check if our similarity matrix works at least in this situation. However, we need to keep in mind that this situation is almost impossible in real life.
  
Make a list of users that rated all of our top K movies.


```python
list_top_k_movies_users = []
for user in range(ratings_matrix.shape[0]):
    if set(list_top_k_movies_indexes).issubset(set(ratings_matrix[user, :].nonzero()[0])):
        list_top_k_movies_users.append(user)
```

Get the ratings these users gave to each of top K similar movies.


```python
user_ratings = {}
for user in list_top_k_movies_users:
    print(ratings_matrix[user, list_top_k_movies_indexes])
    user_ratings[user_by_index[user]] = ratings_matrix[user, list_top_k_movies_indexes]

```

    [ 0.6  0.8  0.8  0.8  1.   0.6]
    [ 0.8  0.6  0.6  1.   0.8  0.8]
    [ 0.8  0.8  1.   0.8  1.   0.8]
    [ 0.8  1.   0.8  0.8  0.8  0.8]
    [ 0.8  0.8  0.8  0.6  1.   0.8]
    [ 0.6  0.6  0.6  0.8  0.8  0.6]
    [ 0.8  0.8  0.6  0.8  0.8  0.8]
    [ 0.4  0.8  0.4  0.4  0.8  0.8]
    [ 0.8  1.   0.6  0.6  0.6  0.6]
    [ 0.8  0.6  1.   0.6  0.4  1. ]
    [ 0.6  0.8  0.6  0.8  0.6  0.6]
    [ 0.8  0.8  0.8  0.8  0.6  0.6]
    [ 0.8  0.6  0.8  0.8  0.8  0.8]
    [ 0.6  0.6  0.8  0.2  0.8  0.6]
    [ 0.4  0.6  0.6  0.4  0.8  0.2]
    [ 0.8  0.8  0.8  0.6  0.8  0.6]
    [ 0.6  1.   0.4  0.8  0.8  0.6]
    [ 0.2  0.2  0.6  0.4  0.4  0.2]
    [ 0.6  0.6  0.4  0.4  0.6  0.6]
    [ 0.6  0.4  0.4  0.6  0.6  0.6]
    [ 0.6  0.6  0.6  0.4  0.6  0.4]
    [ 1.   1.   0.8  0.8  0.6  0.6]
    [ 0.4  0.4  0.4  0.4  0.4  0.2]
    [ 0.4  0.4  0.6  0.4  0.6  0.4]
    [ 0.8  1.   0.8  0.6  0.8  0.8]
    [ 0.6  0.4  0.4  0.8  0.4  0.8]
    [ 0.8  1.   0.8  0.8  0.8  0.6]
    [ 0.6  0.6  0.8  0.4  1.   0.8]
    [ 0.8  0.6  0.8  1.   1.   0.8]
    [ 0.2  0.6  0.6  0.6  0.4  0.8]
    [ 0.4  1.   0.4  0.2  0.6  1. ]
    [ 0.8  0.8  1.   0.6  0.8  1. ]
    [ 0.6  0.6  1.   0.4  0.8  0.8]
    [ 0.6  1.   0.6  0.6  1.   0.8]
    [ 0.8  0.6  0.8  0.8  0.8  0.6]
    [ 1.   0.6  0.6  1.   0.8  0.8]
    [ 0.6  0.8  0.4  0.8  0.4  0.4]
    [ 0.6  1.   1.   0.8  0.8  0.6]
    [ 0.6  0.6  0.4  0.8  1.   0.4]
    [ 0.6  0.4  0.4  0.6  0.8  0.6]
    [ 0.4  0.8  0.8  0.6  0.8  0.6]
    [ 0.6  0.4  1.   1.   0.2  0.6]
    [ 0.8  0.8  0.4  0.6  0.8  0.8]
    [ 0.8  1.   0.2  1.   1.   0.6]
    [ 1.   0.6  0.6  0.6  0.8  0.8]
    [ 0.4  0.6  0.6  0.2  0.6  0.4]
    [ 0.4  0.4  0.4  0.4  0.6  0.4]
    [ 0.6  0.2  0.6  0.6  0.4  0.6]
    [ 0.6  0.8  0.4  0.4  0.6  0.6]
    [ 0.6  0.8  0.8  0.4  0.8  0.6]
    [ 0.6  0.6  0.4  0.6  0.8  0.4]
    [ 0.8  0.6  1.   0.6  0.6  1. ]
    [ 0.6  0.8  0.8  0.2  0.8  0.6]
    [ 0.6  0.4  0.6  1.   0.8  0.4]
    [ 0.6  0.8  0.6  0.2  0.8  0.6]
    [ 0.6  0.4  0.8  0.8  1.   0.6]
    [ 0.6  0.8  0.6  0.6  0.6  0.6]
    [ 1.   1.   1.   0.6  0.4  1. ]
    [ 0.4  0.6  0.4  0.4  0.4  0.2]
    [ 0.6  0.6  1.   0.8  1.   0.6]
    [ 0.4  1.   1.   0.6  0.6  0.8]
    [ 0.4  0.8  0.6  0.4  0.6  0.6]
    [ 1.   1.   0.8  1.   1.   0.6]
    [ 0.8  1.   0.8  0.8  1.   1. ]
    [ 0.6  0.6  0.6  0.8  0.6  0.4]
    [ 0.6  0.8  0.6  0.4  1.   0.6]
    [ 0.6  0.8  0.6  0.6  0.6  1. ]
    [ 1.   1.   1.   0.8  1.   0.8]
    [ 0.4  0.8  0.8  0.6  0.6  0.4]
    [ 0.4  1.   0.4  0.4  0.6  0.6]
    [ 0.8  0.8  0.8  0.6  0.6  0.6]
    [ 1.   1.   0.8  1.   0.8  0.8]
    [ 0.8  0.6  0.4  1.   0.8  0.2]
    [ 0.8  0.8  0.8  0.8  0.8  0.4]
    [ 0.6  0.6  0.8  0.4  0.8  0.6]
    [ 0.6  0.6  0.4  1.   0.8  0.6]
    [ 0.6  1.   0.6  0.8  0.6  1. ]
    [ 0.8  0.6  0.6  0.6  0.8  0.4]
    [ 0.6  1.   0.6  0.8  0.6  0.6]
    [ 1.   0.8  0.6  0.8  0.8  0.6]
    [ 0.2  0.8  0.6  0.4  0.8  0.2]
    [ 0.2  0.2  0.2  0.8  0.4  0.2]
    [ 0.6  0.8  0.8  0.6  0.8  0.4]
    [ 1.   0.8  1.   1.   1.   1. ]
    [ 1.   1.   1.   1.   0.8  1. ]
    [ 0.6  0.6  0.4  1.   0.8  0.6]
    [ 0.8  0.8  0.6  0.8  0.8  0.8]
    [ 0.6  0.8  0.6  0.8  1.   0.8]
    [ 1.   1.   1.   0.6  0.8  0.2]
    [ 0.8  0.8  0.8  0.6  0.8  0.6]
    [ 0.6  0.6  0.8  0.6  0.6  0.6]
    [ 0.2  0.4  0.4  1.   0.8  0.4]
    [ 0.6  0.6  0.8  0.4  0.8  0.4]
    [ 0.8  1.   0.8  0.6  0.8  0.6]
    [ 0.8  0.8  0.8  1.   0.6  0.4]
    [ 0.6  0.8  0.4  0.4  1.   0.4]
    [ 1.   0.6  0.6  1.   0.8  0.6]
    [ 0.6  0.6  0.4  0.4  0.6  0.4]
    [ 0.6  0.8  0.8  0.4  0.8  0.2]
    [ 0.2  0.6  0.6  0.4  0.6  0.4]
    [ 0.6  0.6  0.4  0.4  0.2  0.4]
    [ 0.6  1.   0.8  0.8  0.8  0.6]
    [ 0.6  0.6  0.6  0.4  0.6  0.6]
    [ 0.8  0.8  0.8  0.8  0.6  0.8]
    [ 0.8  1.   0.8  0.6  1.   0.8]
    [ 0.8  1.   1.   0.4  1.   0.6]
    [ 0.8  0.8  0.6  0.8  0.6  0.4]
    [ 0.6  0.6  0.8  0.4  0.6  0.6]
    [ 0.6  0.6  0.6  1.   0.6  0.4]
    [ 1.   1.   0.4  0.6  1.   0.8]
    [ 0.6  0.8  0.6  0.4  0.8  0.4]
    [ 0.2  0.6  1.   0.4  0.6  0.8]
    [ 1.   0.8  1.   0.6  0.8  0.4]
    [ 0.8  0.8  1.   0.4  0.6  1. ]
    [ 0.4  1.   0.6  0.2  0.8  0.6]
    [ 0.6  0.6  0.4  0.4  0.2  0.2]
    [ 0.6  0.8  0.4  1.   1.   1. ]
    [ 0.8  0.4  0.2  1.   0.2  0.4]
    [ 0.8  1.   1.   1.   0.8  1. ]
    [ 0.8  1.   0.6  0.6  0.8  0.8]
    [ 0.6  0.8  0.6  0.8  0.8  0.6]
    [ 0.8  0.8  0.6  0.8  0.8  0.6]
    [ 0.8  0.6  1.   0.8  0.4  0.4]
    [ 0.8  0.4  0.6  0.8  1.   0.8]
    [ 0.8  0.8  1.   0.4  0.2  0.4]
    [ 0.6  0.6  0.4  0.6  0.6  0.4]
    [ 0.4  0.8  0.6  0.4  0.6  0.2]
    [ 0.8  0.8  0.8  0.8  0.8  0.4]
    [ 0.6  0.6  0.6  0.6  0.6  0.6]
    [ 0.8  0.8  0.8  0.8  1.   0.6]
    [ 0.8  0.6  0.8  0.2  1.   0.6]
    [ 0.4  0.8  0.6  0.4  0.8  0.2]
    [ 0.8  0.6  0.8  0.6  0.8  0.8]
    [ 0.8  0.6  1.   0.4  0.6  0.8]
    [ 0.6  0.6  0.8  0.6  1.   0.6]
    [ 0.6  0.8  0.8  1.   0.8  1. ]
    [ 0.8  0.6  0.4  0.2  0.8  0.6]
    [ 0.6  0.8  0.6  0.6  0.8  0.4]
    [ 0.4  0.8  1.   0.6  1.   0.6]
    [ 0.6  0.8  0.6  0.8  0.8  0.8]
    [ 0.6  0.8  0.6  1.   0.8  1. ]
    [ 0.6  0.8  1.   0.8  0.8  0.6]
    [ 0.6  0.8  0.6  0.8  0.8  0.8]
    [ 0.6  0.8  0.8  0.4  0.6  0.6]
    [ 0.4  0.6  0.6  0.6  0.6  0.4]
    [ 0.8  0.8  0.8  0.6  0.6  0.8]
    [ 0.6  0.8  0.6  0.8  0.6  0.6]
    [ 0.6  0.8  0.6  0.6  0.4  0.6]
    [ 0.2  0.6  0.2  0.2  0.4  0.2]
    [ 0.6  0.8  0.6  0.8  0.6  0.8]
    

Let's print out prediction ratings and true ratings side by side, and visualy compare them.


```python
# idx the movie (i) user (u) wnats to rate
# list_top_k_movies_indexes[1:] - movie indexes withou the 0th because we want to evaluate it
list_of_correlations_for_movies = item_correlation[idx,list_top_k_movies_indexes[1:]]

# lets see first user [ 0.6  0.8  0.8  0.8  1.   0.6]
predicted_rating = []
real_rating = []
for user, ratings_list in user_ratings.items():
    rating = 0
    for r,c in zip(ratings_list[1:],list_of_correlations_for_movies):
        rating += r*c
    rating /= np.sum(list_of_correlations_for_movies)
    predicted_rating.append(r)
    real_rating.append(ratings_list[0])
    print(f'prediction: {r} VS. real: {ratings_list[0]}')
```

    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.4
    prediction: 0.6 VS. real: 0.8
    prediction: 1.0 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.2 VS. real: 0.4
    prediction: 0.6 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.2 VS. real: 0.2
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 1.0
    prediction: 0.2 VS. real: 0.4
    prediction: 0.4 VS. real: 0.4
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.8 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.2
    prediction: 1.0 VS. real: 0.4
    prediction: 1.0 VS. real: 0.8
    prediction: 0.8 VS. real: 0.6
    prediction: 0.8 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.8 VS. real: 1.0
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.4
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.8
    prediction: 0.8 VS. real: 1.0
    prediction: 0.4 VS. real: 0.4
    prediction: 0.4 VS. real: 0.4
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.6
    prediction: 1.0 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 1.0 VS. real: 1.0
    prediction: 0.2 VS. real: 0.4
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.4
    prediction: 0.6 VS. real: 0.4
    prediction: 0.6 VS. real: 1.0
    prediction: 1.0 VS. real: 0.8
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 1.0 VS. real: 0.6
    prediction: 0.8 VS. real: 1.0
    prediction: 0.4 VS. real: 0.4
    prediction: 0.6 VS. real: 0.4
    prediction: 0.6 VS. real: 0.8
    prediction: 0.8 VS. real: 1.0
    prediction: 0.2 VS. real: 0.8
    prediction: 0.4 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 1.0 VS. real: 0.6
    prediction: 0.4 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 1.0
    prediction: 0.2 VS. real: 0.2
    prediction: 0.2 VS. real: 0.2
    prediction: 0.4 VS. real: 0.6
    prediction: 1.0 VS. real: 1.0
    prediction: 1.0 VS. real: 1.0
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.6
    prediction: 0.2 VS. real: 1.0
    prediction: 0.6 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.2
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.4 VS. real: 0.8
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 1.0
    prediction: 0.4 VS. real: 0.6
    prediction: 0.2 VS. real: 0.6
    prediction: 0.4 VS. real: 0.2
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.8
    prediction: 0.4 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.6
    prediction: 0.8 VS. real: 1.0
    prediction: 0.4 VS. real: 0.6
    prediction: 0.8 VS. real: 0.2
    prediction: 0.4 VS. real: 1.0
    prediction: 1.0 VS. real: 0.8
    prediction: 0.6 VS. real: 0.4
    prediction: 0.2 VS. real: 0.6
    prediction: 1.0 VS. real: 0.6
    prediction: 0.4 VS. real: 0.8
    prediction: 1.0 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.4 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.4 VS. real: 0.8
    prediction: 0.4 VS. real: 0.6
    prediction: 0.2 VS. real: 0.4
    prediction: 0.4 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.6 VS. real: 0.8
    prediction: 0.2 VS. real: 0.4
    prediction: 0.8 VS. real: 0.8
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 1.0 VS. real: 0.6
    prediction: 0.6 VS. real: 0.8
    prediction: 0.4 VS. real: 0.6
    prediction: 0.6 VS. real: 0.4
    prediction: 0.8 VS. real: 0.6
    prediction: 1.0 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.8 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.4 VS. real: 0.4
    prediction: 0.8 VS. real: 0.8
    prediction: 0.6 VS. real: 0.6
    prediction: 0.6 VS. real: 0.6
    prediction: 0.2 VS. real: 0.2
    prediction: 0.8 VS. real: 0.6
    

Seems preatty good. But, let's calculate our MSE and MAE errors.


```python
MSE = mean_squared_error(real_rating, predicted_rating)
MAE = mean_absolute_error(real_rating, predicted_rating)
print(f'MSE = {MSE}, MAE = {MAE}')
```

    MSE = 0.054666666666666676, MAE = 0.16133333333333336
    

Output seems very good.  
However, we need to expect a larger error when implementing this algorithm to random users.


```python

```

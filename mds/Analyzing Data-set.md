
# Analysing Information from Movie Dataset

## Introduction

In this project, the data from movies, users and ratings was analised in order to acomplish the movie rating prediction task.
  
We will focus on *collaborative filtering* models, which can be generally split into two classes: user-based collaborative filtering and item-based collaborative filtering. In either scenario, one builds a similarity matrix. For item-based collaborative filtering (in this case is movie-based), the item-similarity matrix will consist of some distance metric that measures the similarity between any two pairs of items. Likewise, the user-similarity matrix will measure the similarity between any two pairs of users.
  
Models are trained with **Item-based K nearest neighbours (KNN)** and **User-based K nearest neighbours (KNN)**.
Afterwards, the error is calculated. By comparing the rate prediction error (*Mean Square Error* - MSE and *Mean Absolute Error* - MAE), we are able to evaluate the performance of the proposed model.  
  
The goal is to build simulation of recommendation system using provided data, which recommends movies to the user to meet their preference as well as possible. Therefore, the main task is to predict how the user will rate movies that he/she has not watched, and recommend those with higher potential ratings.

## Dataset

Dataset is downloaded from the following link: [movie recommend data](http://www.vukmalbasa.com/poslovna-inteligencija/file/movie%20recommend%20data.zip?attredirects=0&d=1).

Dataset format:  
    1. Movie.txt
    2. Peson.txt 
    3. Vote.txt


Let's look inside the files. But first we need to load some dependencies.


```python
# load dependencies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
```

### Load data

We will load the data and store it in a pandas dataframe. Why we will use pandas? Because the fundamental behaviour about data types, indexing, and axis labeling apply across all of the objects. We can name columns and manipulate data easily, and the links we make between data will stay consistent unless we change it explicitly.


```python
data_dir = 'data/'
```


```python
# users file
user_columns = ['user_id', 'age', 'sex', 'zip_code']
users = pd.read_csv(data_dir + 'Person.txt', sep='\t', names=user_columns, encoding='latin-1')
```


```python
# ratings file
vote_columns = ['user_id', 'movie_id', 'rating1', 'rating2', 'unix_timestamp']
votes = pd.read_csv(data_dir + 'Vote.txt', sep='\t', names=vote_columns, encoding='latin-1')
```


```python
# movies file
movie_columns = ['movie_id', 'movie_title', 'site_link', 'IMDb_URL', 'release_date', 'release_date_relative', 'video_release_date', 'video_release_date_relative', 'unknown', 'Action', 'Adventure',
 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy']
movies = pd.read_csv(data_dir + 'Movie.txt', sep='\t', names=movie_columns, encoding='latin-1')
```

We will create merged table that consists of all the read data combined. We are merging this data in order to make some interesting plots easier later.


```python
# merge users, movies, and votes data
movie_ratings = pd.merge(movies, votes)
merged_table = pd.merge(movie_ratings, users)
```

### Tables


```python
# users
users.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>age</th>
      <th>sex</th>
      <th>zip_code</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>1</th>
      <td>10</td>
      <td>38.0</td>
      <td>F</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>2</th>
      <td>16</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>17</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>18</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



We see that the only information we get about the user is their ID, age, gender and probably zip code. Zip code will not be used for extracting any relevant information later. We also see that some of users don't have specified age, gender nor zip code.


```python
# votes
votes.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>movie_id</th>
      <th>rating1</th>
      <th>rating2</th>
      <th>unix_timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>0.6</td>
      <td>1.0</td>
      <td>4/15/96 13:16:00</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>2</td>
      <td>0.6</td>
      <td>1.0</td>
      <td>10/22/96 16:37:31</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>10</td>
      <td>0.4</td>
      <td>1.0</td>
      <td>10/26/96 15:07:36</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>13</td>
      <td>0.0</td>
      <td>0.2</td>
      <td>4/15/96 12:13:36</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>17</td>
      <td>0.8</td>
      <td>1.0</td>
      <td>9/18/96 9:22:08</td>
    </tr>
  </tbody>
</table>
</div>



In the votes dataset, we can see that every user-movie relation is described additionaly with the **rating1** rating that user gave to that movie (on the scale from 0 to 1), **rating2** time that user spent before rating the movie, and timestamp when the user rated the movie. User-movie relation is represented using their IDs. 


```python
# movies
movies.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>movie_id</th>
      <th>movie_title</th>
      <th>site_link</th>
      <th>IMDb_URL</th>
      <th>release_date</th>
      <th>release_date_relative</th>
      <th>video_release_date</th>
      <th>video_release_date_relative</th>
      <th>unknown</th>
      <th>Action</th>
      <th>Adventure</th>
      <th>Animation</th>
      <th>Children's</th>
      <th>Comedy</th>
      <th>Crime</th>
      <th>Documentary</th>
      <th>Drama</th>
      <th>Fantasy</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Toy Story</td>
      <td>http://www.toystory.com</td>
      <td>http://us.imdb.com/M/title-exact?Toy%20Story%2...</td>
      <td>old</td>
      <td>NaN</td>
      <td>current</td>
      <td>10/30/96 0:00:00</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Jumanji</td>
      <td>http://www.spe.sony.com/Pictures/SonyMovies/Ju...</td>
      <td>http://us.imdb.com/M/title-exact?Jumanji%20(1995)</td>
      <td>old</td>
      <td>NaN</td>
      <td>old</td>
      <td>5/14/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>Grumpier Old Men</td>
      <td>NaN</td>
      <td>http://us.imdb.com/M/title-exact?Grumpier%20Ol...</td>
      <td>old</td>
      <td>NaN</td>
      <td>current</td>
      <td>6/4/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>Waiting to Exhale</td>
      <td>NaN</td>
      <td>http://us.imdb.com/M/title-exact?Waiting%20to%...</td>
      <td>old</td>
      <td>1/15/96 0:00:00</td>
      <td>old</td>
      <td>4/23/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>Father of the Bride Part II</td>
      <td>http://www.movienet.com/movienet/movinfo/fathe...</td>
      <td>http://us.imdb.com/M/title-exact?Father%20of%2...</td>
      <td>old</td>
      <td>NaN</td>
      <td>current</td>
      <td>6/11/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



Movie datasets consist of movies described with their ID, title, link to the movie, link to the IMDB, relative release date and the exact release date, video release date and the exact video release date, and 10 genres (1-True, 0-False).


```python
# merged table
merged_table.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>movie_id</th>
      <th>movie_title</th>
      <th>site_link</th>
      <th>IMDb_URL</th>
      <th>release_date</th>
      <th>release_date_relative</th>
      <th>video_release_date</th>
      <th>video_release_date_relative</th>
      <th>unknown</th>
      <th>Action</th>
      <th>...</th>
      <th>Documentary</th>
      <th>Drama</th>
      <th>Fantasy</th>
      <th>user_id</th>
      <th>rating1</th>
      <th>rating2</th>
      <th>unix_timestamp</th>
      <th>age</th>
      <th>sex</th>
      <th>zip_code</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Toy Story</td>
      <td>http://www.toystory.com</td>
      <td>http://us.imdb.com/M/title-exact?Toy%20Story%2...</td>
      <td>old</td>
      <td>NaN</td>
      <td>current</td>
      <td>10/30/96 0:00:00</td>
      <td>0</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0.6</td>
      <td>1.0</td>
      <td>4/15/96 13:16:00</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Jumanji</td>
      <td>http://www.spe.sony.com/Pictures/SonyMovies/Ju...</td>
      <td>http://us.imdb.com/M/title-exact?Jumanji%20(1995)</td>
      <td>old</td>
      <td>NaN</td>
      <td>old</td>
      <td>5/14/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0.6</td>
      <td>1.0</td>
      <td>10/22/96 16:37:31</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>2</th>
      <td>10</td>
      <td>Goldeneye</td>
      <td>http://www.mgmua.com/bond/index.html</td>
      <td>http://us.imdb.com/M/title-exact?GoldenEye%20(...</td>
      <td>old</td>
      <td>NaN</td>
      <td>old</td>
      <td>5/21/96 0:00:00</td>
      <td>1</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0.4</td>
      <td>1.0</td>
      <td>10/26/96 15:07:36</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>3</th>
      <td>13</td>
      <td>Balto</td>
      <td>http://www.mca.com/ppv/balto/index.html</td>
      <td>http://us.imdb.com/M/title-exact?Balto%20(1995)</td>
      <td>old</td>
      <td>NaN</td>
      <td>old</td>
      <td>4/5/96 0:00:00</td>
      <td>0</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0.0</td>
      <td>0.2</td>
      <td>4/15/96 12:13:36</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
    <tr>
      <th>4</th>
      <td>17</td>
      <td>Sense and Sensibility</td>
      <td>http://www.spe.sony.com/Pictures/SonyMovies/se...</td>
      <td>http://us.imdb.com/M/title-exact?Sense%20and%2...</td>
      <td>old</td>
      <td>NaN</td>
      <td>old</td>
      <td>6/25/96 0:00:00</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0.8</td>
      <td>1.0</td>
      <td>9/18/96 9:22:08</td>
      <td>42.0</td>
      <td>M</td>
      <td>94025</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>



As told before, this merged table is just combination of data from previous three datasets. Therefore, there is no need of explaining the data twice.


```python
print(list(merged_table))
```

    ['movie_id', 'movie_title', 'site_link', 'IMDb_URL', 'release_date', 'release_date_relative', 'video_release_date', 'video_release_date_relative', 'unknown', 'Action', 'Adventure', 'Animation', "Children's", 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'user_id', 'rating1', 'rating2', 'unix_timestamp', 'age', 'sex', 'zip_code']
    

### Total number of users, movies, and votes 

Now, let's see some numbers. What is the amount of data we have?


```python
total_users = users.shape[0]
print(f'Total number of users: {total_users}')
```

    Total number of users: 72916
    


```python
total_movies = movies.shape[0]
print(f'Total number of movies: {total_movies}')
```

    Total number of movies: 1628
    


```python
total_votes = votes.shape[0]
print(f'Total number of votes: {total_votes}')
```

    Total number of votes: 2811983
    

Let's keep in mind that:
- some of users that haven't voted. 
- some of users voted several times (for a different movie). 

#### Voting information

How many users voted? 


```python
total_voters = votes.user_id.unique().shape[0]
print(f'Number of users who voted: {total_voters}')
```

    Number of users who voted: 61265
    

Here we can see that **84%** of users voted for at least one movie.

How many movies have at least one vote?


```python
total_voted_movies = votes.movie_id.unique().shape[0]
print(f'Number of movies users voted for: {total_voted_movies}')
```

    Number of movies users voted for: 1623
    

We can see that users were really busy. Only 5 movies don't have the rating, so the rest of **99.69%** movies have at least one rating.

### What are the top 20 movies?

What are the top 20 movies based on the number of user who gave them the rating?


```python
merged_table.movie_title.value_counts()[:20]
```




    Batman (1989)                 32868
    Dances With Wolves            32677
    Apollo 13                     31263
    Pulp Fiction                  30515
    True Lies                     29383
    Ace Ventura: Pet Detective    25994
    Aladdin                       24378
    The Fugitive                  24281
    Batman Forever                24262
    Die Hard: With a Vengeance    24003
    Independence Day (ID4)        23605
    Clear and Present Danger      22363
    Forrest Gump                  21801
    The Silence of the Lambs      21729
    Twister                       21400
    Dumb and Dumber               21024
    Toy Story                     20725
    12 Monkeys                    20635
    Beauty and the Beast          20584
    Jurassic Park                 20560
    Name: movie_title, dtype: int64



We can see that movie *Batman (1989)* received the largest amount of ratings (almost **46%** of the users from this dataset rated for this movie).

What are the top 5 movies based on the time user spent before rating it?


```python
rating_time = merged_table.groupby('movie_title').agg({'rating2': [np.sum]})
rating_time.sort_values([('rating2', 'sum')], ascending=False).head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th>rating2</th>
    </tr>
    <tr>
      <th></th>
      <th>sum</th>
    </tr>
    <tr>
      <th>movie_title</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Batman (1989)</th>
      <td>32443.9</td>
    </tr>
    <tr>
      <th>Dances With Wolves</th>
      <td>32211.3</td>
    </tr>
    <tr>
      <th>Apollo 13</th>
      <td>31053.6</td>
    </tr>
    <tr>
      <th>Pulp Fiction</th>
      <td>29625.4</td>
    </tr>
    <tr>
      <th>True Lies</th>
      <td>28783.8</td>
    </tr>
  </tbody>
</table>
</div>



It seems that movies are in the same order as above, which can be explained that Batman movie has the largest amount of ratings so all these user have to spent some tome before rating it.

But, let's see what are the possible times spent on movie before rating it on the site.


```python
merged_table.rating2.value_counts()
```




    1.0    2559107
    0.2     223134
    0.5      29742
    Name: rating2, dtype: int64



We can see that the values are in range from 0 to 1. That means that we don't have the exact time in minutes (nor hours), it seems relative to something. But, let's conclude that the higher the relative number is, the greater is the time spent on movie before rating it.


```python
merged_table.rating2.plot.hist(bins=3)
plt.title("Distribution of users' timings before rating a movie")
plt.ylabel('count of users')
plt.xlabel('time spent');
plt.xlim([0,1])
plt.show()
```


![png](output_49_0.png)


Visual representation of time users spent before rating the movie. It seems that a lot of ratings are given with a careful thought.


```python
merged_table.rating1.value_counts()
```




    0.8    761676
    0.6    701236
    1.0    511667
    0.0    347191
    0.4    339718
    0.2    150495
    Name: rating1, dtype: int64



Here we can see that **27%** of ratings are 0.8 (the largest amount of ratings), **18%** are 1.0 (lovers), and **12.4%** are 0.0 (haters).


```python
merged_table.rating1.plot.hist(bins=6)
plt.title("Distribution of users' rates")
plt.ylabel('count of users')
plt.xlabel('rating');
plt.xlim([0,1])
plt.show()
```


![png](output_53_0.png)


Visual representation of user ratings.

### Which movies are most highly rated?

First, calculate the average rating of the each movie.


```python
movie_stats = merged_table.groupby('movie_title').agg({'rating1': [np.size, np.mean]})
movie_stats.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="2" halign="left">rating1</th>
    </tr>
    <tr>
      <th></th>
      <th>size</th>
      <th>mean</th>
    </tr>
    <tr>
      <th>movie_title</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>'Til There Was You</th>
      <td>111.0</td>
      <td>0.450450</td>
    </tr>
    <tr>
      <th>1-900</th>
      <td>179.0</td>
      <td>0.320670</td>
    </tr>
    <tr>
      <th>101 Dalmatians (1996)</th>
      <td>3358.0</td>
      <td>0.566289</td>
    </tr>
    <tr>
      <th>12 Angry Men (1957)</th>
      <td>624.0</td>
      <td>0.837500</td>
    </tr>
    <tr>
      <th>12 Monkeys</th>
      <td>20635.0</td>
      <td>0.691078</td>
    </tr>
  </tbody>
</table>
</div>



Here we can see that, for example, movie *'Til There Was You* has 111 ratings and the average rating of the movie is 0.45. The same way of reading information implies for the rest of the movies.

Now, we can sort movies by their average grade (from the best grade)


```python
movie_stats.sort_values([('rating1', 'mean')], ascending=False).head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="2" halign="left">rating1</th>
    </tr>
    <tr>
      <th></th>
      <th>size</th>
      <th>mean</th>
    </tr>
    <tr>
      <th>movie_title</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Schindler's List</th>
      <td>12881.0</td>
      <td>0.878410</td>
    </tr>
    <tr>
      <th>The Wrong Trousers</th>
      <td>957.0</td>
      <td>0.877325</td>
    </tr>
    <tr>
      <th>The Shawshank Redemption</th>
      <td>19730.0</td>
      <td>0.867592</td>
    </tr>
    <tr>
      <th>Casablanca (1942)</th>
      <td>1579.0</td>
      <td>0.865611</td>
    </tr>
    <tr>
      <th>Raiders of the Lost Ark (1981)</th>
      <td>2825.0</td>
      <td>0.863646</td>
    </tr>
  </tbody>
</table>
</div>



Movie *Schindler's List* has the highest average rating - 0.87 - in this dataset. The second movie in this list is *The Wrong Trousers* whit rating almost as high as *Schindler's List*.  

### What is the age of movie-likers?

Now, let's see the age distribution of users in our dataset.


```python
users.age.plot.hist(bins=300)
plt.title("Distribution of users' ages")
plt.ylabel('count of users')
plt.xlabel('age');
plt.xlim([0,100])
plt.show()
```


![png](output_64_0.png)


On this plot we can see that more than 8000 users didn't specify their age. However, if we ignore them for a moment, we can see that largest amount users are in their **20s**.  
It is interesting to see that there are even a few people in their **100s** who rated some movies. Wow! 

#### Group users by their age

In order to easily find out interestig information related to users' ages in dataset, it is better to group users in age groups (users that have between 30-39), rather than having a few users within a certain age (users that have 37).


```python
# be sure that the index is back to the initial
merged_table.reset_index('index', inplace=True)
```


```python
labels = ['0-9', '10-19', '20-29', '30-39', '40-49', '50-59', '60-69', '70-79', '80-89', '90-99', '100-109']
merged_table['age_group'] = pd.cut(merged_table.age, range(0, 111, 10), right=False, labels=labels)
merged_table[['age', 'age_group']].drop_duplicates()[:10]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>age_group</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>42.0</td>
      <td>40-49</td>
    </tr>
    <tr>
      <th>465</th>
      <td>38.0</td>
      <td>30-39</td>
    </tr>
    <tr>
      <th>553</th>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>745</th>
      <td>47.0</td>
      <td>40-49</td>
    </tr>
    <tr>
      <th>1752</th>
      <td>29.0</td>
      <td>20-29</td>
    </tr>
    <tr>
      <th>5011</th>
      <td>28.0</td>
      <td>20-29</td>
    </tr>
    <tr>
      <th>5200</th>
      <td>36.0</td>
      <td>30-39</td>
    </tr>
    <tr>
      <th>5203</th>
      <td>0.0</td>
      <td>0-9</td>
    </tr>
    <tr>
      <th>5234</th>
      <td>26.0</td>
      <td>20-29</td>
    </tr>
    <tr>
      <th>5364</th>
      <td>53.0</td>
      <td>50-59</td>
    </tr>
  </tbody>
</table>
</div>



Here we can see how the selection is done. We are appending new column *age_group* to our dataset.

### What is the number of votes per user group?


```python
merged_table.groupby('age_group').agg({'rating1': [np.size, np.mean],'rating2': [np.size, np.mean] })
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr>
      <th></th>
      <th colspan="2" halign="left">rating1</th>
      <th colspan="2" halign="left">rating2</th>
    </tr>
    <tr>
      <th></th>
      <th>size</th>
      <th>mean</th>
      <th>size</th>
      <th>mean</th>
    </tr>
    <tr>
      <th>age_group</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0-9</th>
      <td>211586.0</td>
      <td>0.585064</td>
      <td>211586.0</td>
      <td>0.924045</td>
    </tr>
    <tr>
      <th>10-19</th>
      <td>363446.0</td>
      <td>0.587902</td>
      <td>363446.0</td>
      <td>0.907236</td>
    </tr>
    <tr>
      <th>20-29</th>
      <td>1005084.0</td>
      <td>0.598543</td>
      <td>1005084.0</td>
      <td>0.927024</td>
    </tr>
    <tr>
      <th>30-39</th>
      <td>603209.0</td>
      <td>0.614717</td>
      <td>603209.0</td>
      <td>0.937480</td>
    </tr>
    <tr>
      <th>40-49</th>
      <td>426180.0</td>
      <td>0.629047</td>
      <td>426180.0</td>
      <td>0.946155</td>
    </tr>
    <tr>
      <th>50-59</th>
      <td>160482.0</td>
      <td>0.645204</td>
      <td>160482.0</td>
      <td>0.957400</td>
    </tr>
    <tr>
      <th>60-69</th>
      <td>27634.0</td>
      <td>0.638243</td>
      <td>27634.0</td>
      <td>0.941485</td>
    </tr>
    <tr>
      <th>70-79</th>
      <td>4173.0</td>
      <td>0.633597</td>
      <td>4173.0</td>
      <td>0.926072</td>
    </tr>
    <tr>
      <th>80-89</th>
      <td>334.0</td>
      <td>0.688024</td>
      <td>334.0</td>
      <td>0.969760</td>
    </tr>
    <tr>
      <th>90-99</th>
      <td>1365.0</td>
      <td>0.582418</td>
      <td>1365.0</td>
      <td>0.949597</td>
    </tr>
    <tr>
      <th>100-109</th>
      <td>559.0</td>
      <td>0.576029</td>
      <td>559.0</td>
      <td>0.935599</td>
    </tr>
  </tbody>
</table>
</div>



Looking at the output, we see that users in age group 20-29 gave the largest number of ratings (1005084). That is not so strange since the largest number of users in dataset is in that age group.  
  
The least numer of ratings (334) comes from age group 80-89. However, this age group seems has the average rating that is the highest (almost 0.7). These users don't waist time on bad movies! This group of user also spent, in average, the most amount of time before rating the movie.  

It is interesting to see that people in the age group 100-109 are giving the worst average rating to movies (0.576029). After them, children between 0-9 give the worst average rating to movies (0.585064).


### What is the number of votes per user group for each movie?

It is always interesting to explore the audience. Let's find out what movies are most popular in which age groups.


```python
most_50 = merged_table.groupby('movie_id').size().sort_values(ascending=False)[:50]
merged_table.set_index('movie_id', inplace=True)
```


```python
# we will use previously collected movies that have the highest rating
by_age = merged_table.loc[most_50.index].groupby(['movie_title', 'age_group'])
by_age.rating1.size().head(44)
```




    movie_title                 age_group
    12 Monkeys                  0-9          1612
                                10-19        2767
                                20-29        7923
                                30-39        4242
                                40-49        2850
                                50-59         986
                                60-69         146
                                70-79          22
                                80-89           4
                                90-99          13
                                100-109         4
    Ace Ventura: Pet Detective  0-9          2097
                                10-19        3351
                                20-29        8972
                                30-39        5672
                                40-49        4101
                                50-59        1450
                                60-69         242
                                70-79          40
                                80-89           2
                                90-99           8
                                100-109         7
    Aladdin                     0-9          1974
                                10-19        3217
                                20-29        8345
                                30-39        5548
                                40-49        3774
                                50-59        1208
                                60-69         216
                                70-79          35
                                80-89           1
                                90-99           8
                                100-109         7
    Apollo 13                   0-9          2818
                                10-19        3401
                                20-29        9570
                                30-39        7124
                                40-49        5500
                                50-59        2203
                                60-69         453
                                70-79          93
                                80-89           6
                                90-99          16
                                100-109         8
    dtype: int64



It is interesting to point out that cartoon *Aladdin* was rated mostly in the age group 30-39.  
  
Also, it seems that children between 0-9 rated these movies as much as people in group age between 40-59.

### Which movies do men and women like?

We can get the answer on this question once and for all. First, prepare the data for ploting.


```python
merged_table.reset_index('movie_id', inplace=True)
```


```python
pivoted = merged_table.pivot_table(index=['movie_id','movie_title'],
                           columns=['sex'],
                           values='rating1',
                           fill_value=0)
#pivoted.M = pivoted.M + pivoted.m
#pivoted.m = ''
pivoted.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sex</th>
      <th></th>
      <th>F</th>
      <th>M</th>
      <th>m</th>
    </tr>
    <tr>
      <th>movie_id</th>
      <th>movie_title</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <th>Toy Story</th>
      <td>0.600000</td>
      <td>0.743780</td>
      <td>0.757090</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>2</th>
      <th>Jumanji</th>
      <td>0.000000</td>
      <td>0.638816</td>
      <td>0.623147</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <th>Grumpier Old Men</th>
      <td>0.000000</td>
      <td>0.541867</td>
      <td>0.541975</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <th>Waiting to Exhale</th>
      <td>0.000000</td>
      <td>0.514851</td>
      <td>0.366667</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <th>Father of the Bride Part II</th>
      <td>0.333333</td>
      <td>0.540575</td>
      <td>0.496906</td>
      <td>0.6</td>
    </tr>
  </tbody>
</table>
</div>



*Note:* htere is a bug in data where probably capital M is probably the same as lower-case m, representing Male. There were problems in uniting these two columns, so I took into an account only the capital letter M column.  
  
We see that movie *Toy Story* was better rated by Male users. And *Jumanji* was rated better by Female users. For these two movies, difference in Male and Female ratings is not big.

Now, let's calculate that difference.


```python
pivoted['diff'] = pivoted.M - pivoted.F
pivoted.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sex</th>
      <th></th>
      <th>F</th>
      <th>M</th>
      <th>m</th>
      <th>diff</th>
    </tr>
    <tr>
      <th>movie_id</th>
      <th>movie_title</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <th>Toy Story</th>
      <td>0.600000</td>
      <td>0.743780</td>
      <td>0.757090</td>
      <td>0.6</td>
      <td>0.013310</td>
    </tr>
    <tr>
      <th>2</th>
      <th>Jumanji</th>
      <td>0.000000</td>
      <td>0.638816</td>
      <td>0.623147</td>
      <td>0.0</td>
      <td>-0.015670</td>
    </tr>
    <tr>
      <th>3</th>
      <th>Grumpier Old Men</th>
      <td>0.000000</td>
      <td>0.541867</td>
      <td>0.541975</td>
      <td>0.0</td>
      <td>0.000108</td>
    </tr>
    <tr>
      <th>4</th>
      <th>Waiting to Exhale</th>
      <td>0.000000</td>
      <td>0.514851</td>
      <td>0.366667</td>
      <td>0.0</td>
      <td>-0.148185</td>
    </tr>
    <tr>
      <th>5</th>
      <th>Father of the Bride Part II</th>
      <td>0.333333</td>
      <td>0.540575</td>
      <td>0.496906</td>
      <td>0.6</td>
      <td>-0.043669</td>
    </tr>
  </tbody>
</table>
</div>




```python
pivoted.reset_index('movie_id', inplace=True)
'movie_id' in pivoted
print(list(pivoted))
```

    ['movie_id', ' ', 'F', 'M', 'm', 'diff']
    

Now we will visualise what we got.


```python
disagreements = pivoted[pivoted.movie_id.isin(most_50.index)]['diff']
disagreements.sort_values().plot(kind='barh', figsize=[9, 15])
plt.title('Male vs. Female Avg. Ratings\n(Difference > 0 = Favored by Men)')
plt.ylabel('Title')
plt.xlabel('Average Rating Difference');
plt.show()
```


![png](output_88_0.png)


The movie favourited by Male audience is **Terminator**. This is not a surprise.
The movie extremely favourited by Female audience is **Sense and Sensibility**. 
The movie liked the same by both genders is **The Firm**.

Let's see how old is this dataset by extracting their release dates from their names. The attempt to use the data from release data column wasn't a success because the most of the values in the column are not defined.


```python
movies['release_date_relative'] = pd.to_datetime(movies['release_date_relative'])
movies['year'], movies['month'] = movies['release_date_relative'].dt.year, movies['release_date_relative'].dt.month

movies['year_parsed'] = movies['IMDb_URL'].str.extract(r"\(([1-9]+)\)", expand=False)
movies.year_parsed = movies.year_parsed.astype(float)
#print(movies.year)
movies.year_parsed.plot.hist(bins=250)
plt.title("Releas rate")
plt.ylabel('number of movies released')
plt.xlabel('year');
plt.xlim([1990,2000])
plt.xticks(range(1990,2000,1))
plt.show()
```


![png](output_91_0.png)


We see that the largest amount of movies in this dataset is released in **1995**. The movies come frome 90s.
